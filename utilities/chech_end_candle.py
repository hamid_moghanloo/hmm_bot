from packages import mt5, datetime


def check_end_candle(symbol, signal_len, last_candle=0):
    price = mt5.copy_rates_from(
        symbol,
        mt5.TIMEFRAME_M1,
        datetime(
            datetime.now().year,
            datetime.now().month,
            datetime.now().day + 2
        ),
        signal_len
    )

    now_candle = price[-1][0]
    if last_candle != now_candle:
        return True
    else:
        return False
