from packages import *


def hmm(close_price=None, open_price=None, high_price=None, low_price=None):

    diff = list()
    y_axis = list()

    for i in range(len(close_price)):
        #     diff.append(open_price[i] - close_price[i])
        diff.append(open_price[i] - close_price[i])
        y_axis.append(high_price[i] - low_price[i])

    diff_dict = {
        "x": diff,
        "y": y_axis
    }
    data = np.array(pd.DataFrame(diff_dict)).reshape(len(diff), 2)
    # diff = np.array(pd.DataFrame(diff_dict))

    n_cluster = 7

    # kmeans cluster load from sklearn
    kmeans = KMeans(n_clusters=n_cluster, random_state=0)
    kmeans.fit_predict(data)

    # create enum from cluster_centers
    old_enum = [(kmeans.cluster_centers_[i][0], i) for i in range(len(kmeans.cluster_centers_))]

    cluster_center_sorted = np.sort(kmeans.cluster_centers_, kind='heapsort', axis=0)

    states = list()

    for i in range(len(kmeans.labels_)):
        center_value = old_enum[kmeans.labels_[i]][0]
        item_index = np.where(cluster_center_sorted == center_value)
        states.append(item_index[0][0])

    matrix_positions = list()

    for i in range(n_cluster):
        for j in range(len(states)):
            if i == states[j]:
                try:
                    matrix_positions.append((states[j], states[j + 1]))
                except:
                    pass

    last_position = states[-1]
    hmm_transition_matrix = np.zeros((n_cluster, n_cluster))

    for position in matrix_positions:
        hmm_transition_matrix[position[0]][position[1]] += 1

    for i in range(len(hmm_transition_matrix)):
        sum_row = np.sum(hmm_transition_matrix[i])
        hmm_transition_matrix[i] = hmm_transition_matrix[i] / sum_row

    trade_state = [
        'strong sell',
        'middle sell',
        'poor sell',
        'nothing',
        'poor buy',
        'middle buy',
        'strong buy'
    ]

    state_matrix = [0 for _ in range(n_cluster)]
    state_matrix[last_position] = 1

    new_state = state_matrix @ hmm_transition_matrix

    new_state = list(new_state)
    choice = np.random.choice(new_state, 1, p=new_state)
    # choice = max(new_state)

    return trade_state[new_state.index(choice)]
