from model.hmm import hmm
from packages import *
from trade.close_position import close_position
from trade.buy import buy
from trade.sell import sell
from utilities.chech_end_candle import check_end_candle


def hmm_run(max_iter, close_price=None, open_price=None, high_price=None, low_price=None):
    trades = []
    for f in range(max_iter):
        trades.append(
            hmm(close_price=close_price,
                open_price=open_price,
                high_price=high_price,
                low_price=low_price
                )
        )

    position = max(trades, key=trades.count)

    if 'buy' in position:
        trade = 'ascending'
    elif 'sell' in position:
        trade = 'descending'
    else:
        trade = 'nothing'
    return trade


def mt5_bot(login, password, symbol='EURUSD', lot=0.1, signal_len=1000):
    if not mt5.initialize(login=login, server="MetaQuotes-Demo", password=password):
        print("initialize() failed, error code =", mt5.last_error())
        quit()
        return False

    symbol_info = mt5.symbol_info(symbol)
    if symbol_info is None:
        print(symbol, "not found, can not call order_check()")
        mt5.shutdown()
        quit()
        return False

    price = mt5.copy_rates_from(
        symbol,
        mt5.TIMEFRAME_M1,
        datetime(
            datetime.now().year,
            datetime.now().month,
            datetime.now().day + 2
        ),
        signal_len
    )

    candle_time = price[-1][0]
    while True:
        if check_end_candle(symbol, signal_len, candle_time):
            break
        time.sleep(1)

    while True:

        price = mt5.copy_rates_from(
            symbol,
            mt5.TIMEFRAME_M1,
            datetime(
                datetime.now().year,
                datetime.now().month,
                datetime.now().day + 2
            ),
            signal_len
        )

        close_price = list()
        open_price = list()
        low_price = list()
        high_price = list()

        for i in range(len(price) - 1):
            open_price.append(price[i][1])
            high_price.append(price[i][2])
            low_price.append(price[i][3])
            close_price.append(price[i][4])

        candle_time = price[-1][0]
        trend = hmm_run(
            max_iter=10,
            close_price=close_price,
            open_price=open_price,
            high_price=high_price,
            low_price=low_price
        )

        if trend == 'ascending':
            buy(lot, symbol=symbol)

        elif trend == 'descending':
            sell(lot, symbol=symbol)
        else:
            print(trend)

        if not check_end_candle(symbol, signal_len, candle_time):
            while True:
                if check_end_candle(symbol, signal_len, candle_time):
                    break
                time.sleep(0.1)

        number_of_close_position = len(mt5.positions_get())
        for i in range(number_of_close_position):
            close_position(
                position_id=int(mt5.positions_get()[0]._asdict()['ticket']),
                lot=mt5.positions_get()[0]._asdict()['volume']
            )

        print('----------------this candle is end----------------')


if __name__ == '__main__':
    mt5_bot(login=56753743, password="yip8oamh", lot=0.5, symbol='EURUSD', signal_len=100)
