from packages import mt5


def sell(lot=0.1, symbol="EURUSD"):
    price = mt5.symbol_info_tick(symbol).bid
    deviation = 5
    request = {
        "action": mt5.TRADE_ACTION_DEAL,
        "symbol": symbol,
        "volume": lot,
        "type": mt5.ORDER_TYPE_SELL,
        "price": price,
        "deviation": deviation,
        "magic": 234000,
        "comment": "python script close",
        "type_time": mt5.ORDER_TIME_GTC,
        "type_filling": mt5.ORDER_FILLING_RETURN,
        "tp": price - 0.00020,
        "sl": price + 0.00008,
    }
    # send a trading request
    result = mt5.order_send(request)
    return result
