from packages import mt5


def close_position(position_id, magic=234000, lot=0.1, symbol="EURUSD", **kwarg):

    if mt5.positions_get()[0]._asdict()['type']:
        position_type = mt5.ORDER_TYPE_BUY
        price = mt5.symbol_info_tick(symbol).ask
    else:
        position_type = mt5.ORDER_TYPE_SELL
        price = mt5.symbol_info_tick(symbol).bid

    deviation = 5
    request = {
        "action": mt5.TRADE_ACTION_DEAL,
        "symbol": symbol,
        "volume": lot,
        "type": position_type,
        "position": position_id,
        "price": price,
        "deviation": deviation,
        "magic": magic,
        "comment": "python script close",
        "type_time": mt5.ORDER_TIME_GTC,
        "type_filling": mt5.ORDER_FILLING_RETURN,

    }
    # send a trading request
    result = mt5.order_send(request)
    return result